//
//  Handle giving the user a new edition of the Master Edition NFT that
//  their loyalty level gives them access to.

import axios from "axios";

//
export default async (req, res) => {
  //  Ensure all requests made to this endpoint are POST
  if (req.method !== "POST")
    return res.status(405).json({ message: "Method not allowed" });

  let { walletPublicKey } = req.body;
  let nftMintAddress = "AAfhPBX3ZQvFmfgVPE2eYf1YqygWgWc92ancyoiBoCBm";

  let response;

  let baseUrl = "https://nvfbvaeh85.execute-api.us-east-1.amazonaws.com/dev";

  response = await axios
    .post(
      `${baseUrl}/api/v1/nft/print-new-edition?userWalletAddress=${walletPublicKey}&nftMintAddress=${nftMintAddress}`
    )
    .catch((e) => console.log(e));

  // if (process.env.NODE_ENV == "development") {
  //   response = await axios
  //     .post(
  //       `${baseUrl}/api/v1/nft/print-new-edition?userWalletAddress=${walletPublicKey}&nftMintAddress=${nftMintAddress}`
  //     )
  //     .catch((e) => console.log(e));
  // } else {
  //   response = await axios
  //     .post(
  //       `${baseUrl}/api/v1/nft/print-new-edition?userWalletAddress=${walletPublicKey}&nftMintAddress=${nftMintAddress}&network=mainnet-beta`
  //     )
  //     .catch((e) => console.log(e));
  // }

  res.status(200).json(response.data);
};
