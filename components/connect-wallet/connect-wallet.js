import { useWallet } from "@solana/wallet-adapter-react";
import {
  WalletConnectButton,
  WalletDisconnectButton,
  WalletModalProvider,
  WalletMultiButton,
} from "@solana/wallet-adapter-react-ui";

import connectWalletStyles from "./connect-wallet.module.scss";

// Default styles that can be overridden by your app
require("@solana/wallet-adapter-react-ui/styles.css");

export default function ConnectWallet() {
  const wallet = useWallet();
  return (
    <div className={connectWalletStyles.wrapper}>
      <center>
        <WalletModalProvider>
          {wallet.connected ? <></> : <WalletMultiButton />}
          <div style={{ height: "1em" }} />
          {!wallet.connected ? <></> : <WalletDisconnectButton>Disconnect wallet</WalletDisconnectButton>}
        </WalletModalProvider>
      </center>
    </div>
  );
}
