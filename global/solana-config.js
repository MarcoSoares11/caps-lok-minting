import { WalletAdapterNetwork } from "@solana/wallet-adapter-base";
import { PublicKey, Connection, clusterApiUrl } from "@solana/web3.js";

// Define the network user will have to connect to
// TODO: We can futurely allow user to pick from dropdown menu depending if they'd like to test feature first
export const network =
  process.env.NODE_ENV == "development"
    ? WalletAdapterNetwork.Devnet
    : WalletAdapterNetwork.Mainnet;

export const connection = new Connection(clusterApiUrl(network));

// Ensure the TreeCloud wallet is kept track of as the wallet RECEIVING the SOL
export const treeCloudWallet = new PublicKey(
  "2SVa7aKhSfWYAVYqsMDBPKkYa2U3gwRyGnHq9vyKAQzr"
);
