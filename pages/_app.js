import "../styles/globals.scss";

import {
  ConnectionProvider,
  useWallet,
  WalletProvider,
} from "@solana/wallet-adapter-react";

import { PhantomWalletAdapter } from "@solana/wallet-adapter-wallets";
import { clusterApiUrl } from "@solana/web3.js";
import { useEffect, useMemo } from "react";

import { network } from "../global/solana-config";

function MyApp({ Component, pageProps }) {
  // Use the network selected to provide the cluster endpoint
  const endpoint = useMemo(() => clusterApiUrl(network), [network]);

  //
  // Specify the wallet(s) being used within the application.
  //
  // At the moment we are keeping only Phantom as the default and only wallet
  //
  const wallets = useMemo(() => [new PhantomWalletAdapter()], []);

  return (
    <ConnectionProvider endpoint={endpoint}>
      <WalletProvider wallets={wallets} autoConnect>
        <Component {...pageProps} />{" "}
      </WalletProvider>
    </ConnectionProvider>
  );
}

export default MyApp;
